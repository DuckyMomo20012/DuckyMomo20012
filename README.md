# Hi, I’m Duong Tien Vinh

![Colorful Geometric Abstract Shapes Gamer Video](https://user-images.githubusercontent.com/64480713/170178949-3b2f98e8-c299-4f94-b311-4f30318d9e6e.gif)
[![gitlab](https://img.shields.io/badge/GitLab-330F63?style=flat-square&logo=gitlab&logoColor=white)](https://gitlab.com/DuckyMomo20012)
&nbsp;
[![gitbook](https://img.shields.io/badge/GitBook-7B36ED?style=flat-square&logo=gitbook&logoColor=white)](https://duckymomo20012.gitbook.io/crypto-learning/)
&nbsp;
[![hackerrank](https://img.shields.io/badge/-Hackerrank-2EC866?style=flat-square&logo=HackerRank&logoColor=white)](https://www.hackerrank.com/tienvinh_duong4)
&nbsp;
[![gmail](https://img.shields.io/badge/Gmail-D14836?style=flat-square&logo=gmail&logoColor=white)](mailto:tienvinh.duong4@gmail.com)
&nbsp;
[![linkedin](https://img.shields.io/badge/LinkedIn-0077B5?style=flat-square&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/duong-tien-vinh)
&nbsp;
[![twitter](https://img.shields.io/badge/Twitter-1DA1F2?style=flat-square&logo=twitter&logoColor=white)](https://twitter.com/duckymomo20012)
&nbsp;
[![telegram](https://img.shields.io/badge/Telegram-2CA5E0?style=flat-square&logo=telegram&logoColor=white)](https://t.me/duckymomo20012)
&nbsp;
[![skype](https://img.shields.io/badge/Skype-00AFF0?style=flat-square&logo=skype&logoColor=white)](https://join.skype.com/invite/xabx5AI61PJc)
&nbsp;
[![discord](https://img.shields.io/badge/Discord-5865F2?style=flat-square&logo=discord&logoColor=white)](https://discordapp.com/users/509778560224067605/)

## 🦄 ABOUT ME:

<table>
    <tr>
        <td>
            <li>
                I am a third-year undergraduate student at <b>VNUHCM-University of Science</b>, majoring in <b>Computer Networks & Cybersecurity</b>.
            </li>
            <li>
                I am currently learning <code>React ⚛️</code> for Front-End Development.
            </li>
            <li>
                Kinda self-motivated learner.
            </li>
            <li>
                Interested in new web technologies, frameworks and libraries.
            </li>
            <li>
                ❤️: 🧑‍💻, 🎵, 🏸.
            </li>
        </td>
        <td>
            <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/avatar.jpg" width="300px" alt="Duong Vinh avatar" title="Hi, nice to meet you! 🤖"/>
        </td>
    </tr>
</table>
## 🤖 TECHNOLOGIES:
<table>
    <thead>
        <tr>
            <th></th>
            <th>Learning/Using</th>
            <th>Interested</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Languages</td>
            <td>
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/file_type_html.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/file_type_html.svg" height="32px" alt="html" title="HTML"/>
                </picture>
                &nbsp;
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/file_type_css.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/file_type_css.svg" height="32px" alt="css" title="CSS"/>
                </picture>
                &nbsp;
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/file_type_js_official.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/file_type_js_official.svg" height="32px" alt="javascript" title="Javascript"/>
                </picture>
                &nbsp;
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/file_type_python.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/file_type_python.svg" height="32px" alt="python" title="Python"/>
                </picture>
                &nbsp;
            </td>
            <td>
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/file_type_typescript_official.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/file_type_typescript_official.svg" height="32px" alt="typescript" title="Typescript"/>
                </picture>
                &nbsp;
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/file_type_graphql.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/file_type_graphql.svg" height="32px" alt="graphql" title="GraphQL"/>
                </picture>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>Database</td>
            <td>
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/file_type_mongo.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/file_type_mongo.svg" height="32px" alt="mongodb" title="MongoDB"/>
                </picture>
                &nbsp;
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/sql_server_logo.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/sql_server_logo.svg" height="32px" alt="sql_server" title="SQL Server"/>
                </picture>
                &nbsp;
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/prisma_logo.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/prisma_logo.svg" height="32px" alt="prisma" title="Prisma"/>
                </picture>
                &nbsp;
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>CI/CD</td>
            <td>
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/file_type_docker.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/file_type_docker.svg" height="32px" alt="dockerfile" title="Docker"/>
                </picture>
                &nbsp;
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/file_type_jenkins.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/file_type_jenkins.svg" height="32px" alt="jenkinsfile" title="Jenkins"/>
                </picture>
                &nbsp;
            </td>
            <td>
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/kubernetes_logo.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/kubernetes_logo.svg" height="32px" alt="kubernetes" title="Kubernetes"/>
                </picture>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>Source Code Management (SCM)</td>
            <td>
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/file_type_git.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/file_type_git.svg" height="32px" alt="git" title="Git"/>
                </picture>
                &nbsp;
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/github_logo.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/github_logo.svg" height="32px" alt="github" title="Github"/>
                </picture>
                &nbsp;
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/gitlab_logo.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/gitlab_logo.svg" height="32px" alt="gitlab" title="Gitlab"/>
                </picture>
                &nbsp;
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>QC/QA</td>
            <td>
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/file_type_eslint.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/file_type_eslint.svg" height="32px" alt="eslint" title="ESLint"/>
                </picture>
                &nbsp;
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/file_type_prettier.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/file_type_prettier.svg" height="32px" alt="prettier" title="Prettier"/>
                </picture>
                &nbsp;
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>Text Editor</td>
            <td>
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/file_type_vscode.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/file_type_vscode.svg" height="32px" alt="vscode" title="VSCode"/>
                </picture>
                &nbsp;
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>Design</td>
            <td>
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/figma_logo.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/figma_logo.svg" height="32px" alt="figma" title="Figma"/>
                </picture>
                &nbsp;
            </td>
            <td>
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/canva_logo.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/canva_logo.svg" height="32px" alt="canva" title="Canva"/>
                </picture>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>Team Collaboration</td>
            <td>
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/notion_logo.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/notion_logo.svg" height="32px" alt="notion" title="Notion"/>
                </picture>
                &nbsp;
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/trello_logo.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/trello_logo.svg" height="32px" alt="trello" title="Trello"/>
                </picture>
                &nbsp;
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/slack_logo.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/slack_logo.svg" height="32px" alt="slack" title="Slack"/>
                </picture>
                &nbsp;
            </td>
            <td>
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/asana_logo.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/asana_logo.svg" height="32px" alt="asana" title="Asana"/>
                </picture>
                &nbsp;
            </td>
        </tr>
    </tbody>
</table>

## ⚙️ FRONT END STACK:

<table>
    <thead>
        <tr>
            <th></th>
            <th>Learning/Using</th>
            <th>Interested</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Framework</td>
            <td align="center">
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/file_type_reactjs.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/file_type_reactjs.svg" height="32px" alt="reactjs" title="React JS"/>
                </picture>
                <p align="center"><code><a href="https://reactjs.org/">React</a></code></p>
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/nextjs_logo.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/nextjs_logo.svg" height="32px" alt="nextjs" title="Next JS"/>
                </picture>
                <p align="center"><code><a href="https://nextjs.org/">NextJS</a></code></p>
            </td>
            <td align="center">
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/solidjs_logo.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/solidjs_logo.svg" height="32px" alt="solidjs" title="Solid JS"/>
                </picture>
                <p align="center"><code><a href="https://www.solidjs.com/">SolidJS</a></code></p>
            </td>
        </tr>
        <tr>
            <td>Routing</td>
            <td align="center">
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/react_router_logo.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/react_router_logo.svg" height="32px" alt="react_router" title="React Router v6"/>
                </picture>
                <p align="center"><code><a href="https://reactrouter.com/docs/en/v6">React Router v6</a></code></p>
            </td>
            <td align="center">
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/react_location_logo.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/react_location_logo.svg" height="32px" alt="react_location" title="React Location"/>
                </picture>
                <p align="center"><code><a href="https://react-location.tanstack.com/">React Location</a></code></p>
            </td>
        </tr>
        <tr>
            <td>State Management</td>
            <td align="center">
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/redux_logo.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/redux_logo.svg" height="32px" alt="redux" title="Redux Toolkit"/>
                </picture>
                <p align="center"><code><a href="https://redux-toolkit.js.org/">Redux Toolkit</a></code></p>
            </td>
            <td align="center">
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/recoil_logo.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/recoil_logo.svg" height="32px" alt="recoil" title="Recoil"/>
                </picture>
                <p align="center"><code><a href="https://recoiljs.org/">Recoil</a></code></p>
                <p>🐻</p>
                <p align="center"><code><a href="https://github.com/pmndrs/zustand">Zustand</a></code></p>
            </td>
        </tr>
        <tr>
            <td>Data Fetching</td>
            <td align="center">
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/react_query_logo.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/react_query_logo.svg" height="32px" alt="react_query" title="React Query"/>
                </picture>
                <p align="center"><code><a href="https://react-query.tanstack.com/">React Query</a></code></p>
            </td>
            <td align="center">
            </td>
        </tr>
        <tr>
            <td>Component Styling</td>
            <td align="center">
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/windi_css_logo.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/windi_css_logo.svg" height="32px" alt="windicss" title="WindiCSS"/>
                </picture>
                <p align="center"><code><a href="https://windicss.org/">WindiCSS</a></code></p>
            </td>
            <td align="center">
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/vanilla_extract_logo.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/vanilla_extract_logo.svg" height="32px" alt="vanilla_extract" title="Vanilla Extract"/>
                </picture>
                <p align="center"><code><a href="https://vanilla-extract.style/">Vanilla Extract</a></code></p>
            </td>
        </tr>
        <tr>
            <td>UI Component Library</td>
            <td align="center">
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/mantine_logo.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/mantine_logo.svg" height="32px" alt="mantine" title="Mantine"/>
                </picture>
                <p align="center"><code><a href="https://mantine.dev/">Mantine</a></code></p>
            </td>
            <td align="center">
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/ant_design_logo.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/ant_design_logo.svg" height="32px" alt="ant_design" title="Ant Design"/>
                </picture>
                <p align="center"><code><a href="https://ant.design/">Ant Design</a></code></p>
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/chakra_logo.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/chakra_logo.svg" height="32px" alt="chakra" title="Chakra UI"/>
                </picture>
                <p align="center"><code><a href="https://chakra-ui.com/">Chakra UI</a></code></p>
            </td>
        </tr>
        <tr>
            <td>Build Tool</td>
            <td align="center">
                <picture>
                    <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/vite_logo.svg">
                    <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/vite_logo.svg" height="32px" alt="vite" title="Vite"/>
                </picture>
                <p align="center"><code><a href="https://vitejs.dev/">Vite</a></code></p>
            </td>
            <td align="center">
            </td>
        </tr>
    </tbody>
</table>

## ☎️ CONNECT WITH ME:

<p>📫: tienvinh.duong4@gmail.com (Primary)</p>
<p>📫: tienvinh.duong2@gmail.com (Secondary)</p>
<a href="mailto:tienvinh.duong4@gmail.com">
    <picture>
        <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/gmail_logo.svg">
        <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/gmail_logo.svg" height="32px" width="32px" alt="gmail" title="Mail me"/>
</a>
</picture>
&nbsp;
<a href="https://www.linkedin.com/in/duong-tien-vinh">
    <picture>
        <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/linkedin_logo.svg">
        <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/linkedin_logo.svg" height="32px" width="32px" alt="linkedin" title="My LinkedIn profile"/>
</a>
</picture>
&nbsp;
<a href="https://twitter.com/duckymomo20012">
    <picture>
        <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/twitter_logo.svg">
        <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/twitter_logo.svg" height="32px" width="32px" alt="twitter" title="My Twitter account"/>
</a>
</picture>
&nbsp;
<a href="https://t.me/duckymomo20012">
    <picture>
        <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/telegram_logo.svg">
        <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/telegram_logo.svg" height="32px" width="32px" alt="telegram" title="My Telegram account"/>
</a>
</picture>
&nbsp;
<a href="https://join.skype.com/invite/xabx5AI61PJc">
    <picture>
        <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/skype_logo.svg">
        <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/skype_logo.svg" height="32px" width="32px" alt="skype" title="My Skype account"/>
</a>
</picture>
&nbsp;
<a href="https://discordapp.com/users/509778560224067605/">
    <picture>
        <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/dark/discord_logo.svg">
        <img src="https://raw.githubusercontent.com/DuckyMomo20012/DuckyMomo20012/main/assets/light/discord_logo.svg" height="32px" width="32px" alt="discord" title="My Dicord account"/>
</a>
</picture>

## 🏆 GITHUB STATS:

<!-- Dark Mode:
    - title_color= #B7C4FF
    - text_color= #E4E1E6
    - icon_color= #E5BAD9
    - bg_color= #45464F

    Top Languages config:
    - layout=compact
    - langs_count=10 -->

<p align="center">
    <picture>
        <source media="(prefers-color-scheme: dark)" srcset="https://github-readme-stats.vercel.app/api?username=DuckyMomo20012&show_icons=true&title_color=B7C4FF&text_color=E4E1E6&icon_color=E5BAD9&bg_color=45464F" alt="Duong Vinh's github stats" title="My statistics">
        <img align="top" width="50%" src="https://github-readme-stats.vercel.app/api?username=DuckyMomo20012&show_icons=true&title_color=B7C4FF&text_color=E4E1E6&icon_color=E5BAD9&bg_color=45464F" alt="Duong Vinh's github stats" title="My statistics"/>
    </picture>
    &nbsp;
    <picture>
        <source media="(prefers-color-scheme: dark)" srcset="https://github-readme-stats.vercel.app/api/top-langs/?username=DuckyMomo20012&layout=compact&langs_count=10&&title_color=B7C4FF&text_color=E4E1E6&icon_color=E5BAD9&bg_color=45464F" alt="Duong Vinh's github most used languages" title="My most used languages">
        <img align="top" width="40%" src="https://github-readme-stats.vercel.app/api/top-langs/?username=DuckyMomo20012&layout=compact&langs_count=10&&title_color=B7C4FF&text_color=E4E1E6&icon_color=E5BAD9&bg_color=45464F" alt="Duong Vinh's github most used languages" title="My most used languages"/>
    </picture>
</p>
